import os

from boss.pp.pp_main import PPMain
from boss.settings import Settings
from boss.io.parse import parse_input_file


def postprocess_sigma():

    input_data = parse_input_file("boss.rst")
    settings = Settings(input_data["keywords"])

    settings["pp_models"] = 1

    pp_main = PPMain.from_settings(settings)
    pp_main.run()
    print("Postprocessing output saved in 'postprocessing/'")


def postprocess_gamma(dir_path):
    os.chdir(dir_path)

    input_data = parse_input_file("boss.rst")

    input_data["keywords"]["rstfile"] = "boss.rst"
    input_data["keywords"]["outfile"] = "boss.out"
    input_data["keywords"]["pp_models"] = 1
    input_data["keywords"]["userfn"] = None

    settings = Settings(input_data["keywords"])
    pp = PPMain.from_settings(settings)
    pp.run()

    os.chdir("..")
    print(f"Postprocessing output saved in '{dir_path}/postprocessing/'")


if __name__ == "__main__":
    # Run postprocessing on the BOSS run for sigma and on the final KRR
    # hyperparameter optimization
    postprocess_sigma()
    postprocess_gamma("gamma_boss_final")
