import sys
import os
import numpy as np

from sklearn.kernel_ridge import KernelRidge
from sklearn.model_selection import cross_val_score

from boss.bo.bo_main import BOMain

sys.path.insert(0, "../../code")
from model import *
from merge_data import *
from init_mbtr import tier_sigma_n_mbtr


def log_cv_mae(X, krr_hp):
    """
    Log of the cross-validated MAE.
    Args:
        X (ndarray): MBTR vectors
        krr_hp (ndarray): KRR hyperparameters
    Returns:
        float: corss-validated log MAE
    """
    global cv_splits
    global y
    global model

    alpha = 10 ** (-5)
    gamma = 10 ** krr_hp[0, 0]

    krr = KernelRidge(kernel="rbf", alpha=alpha, gamma=gamma)

    cv = cross_val_score(
        krr, X, y, scoring="neg_mean_absolute_error", cv=cv_splits, n_jobs=1
    )
    cv_mae = -np.mean(cv)
    cv_mae_etot = cv_mae / model.transformation.scale()
    cv_mae_etot_log = np.log10(cv_mae_etot)

    print("  {} {} {}".format(krr_hp[0], cv_mae_etot_log, cv_mae_etot), flush=True)

    return cv_mae_etot_log


def gamma_opt_boss(sigma, output_dir):
    """
    Performs a 1D BOSS run to optimize log_cv_mae() over the KRR hyperparameter
    gamma  with a given sigma value.
    Args:
        sigma (float): MBTR hyperparameter sigma
        output_dir (string): path for the BOSS output directory
    Returns:
        BOResults object: BOSS run
    """

    # Creating a directory for the BOSS outputs
    os.mkdir(output_dir)

    # Defining the MBTR representation
    mbtr = tier_sigma_n_mbtr(1, sigma, 50, normalization="none")

    print("  Calculating MBTR vectors", flush=True)
    X = mbtr.create(s, n_jobs=4)

    # Defining the optimized function by setting the MBTR vectors
    f = lambda krr_hp: log_cv_mae(X, krr_hp)

    print("  Optimizing KRR hyperparameters:", flush=True)
    bo = BOMain(
        f,
        np.array([[-12, 0]]),  # bounds
        yrange=[-4, 0],
        kernel="rbf",
        initpts=5,
        iterpts=15,
        noise=1e-6,
        # acqtol=1e-4,
        verbosity=2,
    )

    # Working around the global output system of BOSS
    bo.settings["rstfile"] = "{}/boss.rst".format(output_dir)
    bo.settings["outfile"] = "{}/boss.out".format(output_dir)
    bo.rst_manager = None
    bo._setup()

    return bo.run()


def cv_mae(mbtr_hp):
    """
    Optimizes the KRR hyperparameter gamma with a 1D BOSS run and returns the
    predicted minimum for the CV MAE.
    Args:
        mbtr_hp: MBTR hyper parameters
    Returns:
        float: predicted minimum of the CV MAE
    """
    global sigma_boss_iterarion
    global s

    sigma = 10.0 ** mbtr_hp[0, 0]

    print(
        "\nIteration {}: log10(sigma) = {} (sigma = {})".format(
            sigma_boss_iterarion, mbtr_hp[0, 0], sigma
        ),
        flush=True,
    )
    output_dir = "gamma_boss_{:0>2}".format(sigma_boss_iterarion)
    boss_run = gamma_opt_boss(sigma, output_dir)

    fmin = boss_run.fmin
    fmin_etot = 10**fmin

    ybest = boss_run.ybest
    ybest_etot = 10**ybest

    print(
        "Best prediction: {} {} {}".format(boss_run.xmin, fmin, fmin_etot), flush=True
    )
    print(
        "Best acquicition: {} {} {}".format(boss_run.xbest, ybest, ybest_etot),
        flush=True,
    )

    sigma_boss_iterarion += 1

    return boss_run.fmin


def sigma_opt_boss():
    """
    Performs a 1D BOSS run to optimize cv_mae() over log(sigma).
    Returns:
        BOResults object: BOSS run
    """
    bo = BOMain(
        cv_mae,
        np.array([[-3, 0]]),  # bounds
        yrange=[-4, 0],
        kernel="rbf",
        initpts=5,
        iterpts=15,
        noise=1e-6,
        # acqtol=1e-4,
        verbosity=2,
    )

    return bo.run()


if __name__ == "__main__":
    import sklearn
    import boss

    sklearn.show_versions()
    print("\nBOSS version: {}".format(boss.__version__))

    print("\nLoading data")
    structures, labels = merged_uniform_data("../../data/")

    # Only using the training data
    tr_mask = labels["train"].values
    s = [st for i, st in enumerate(structures) if tr_mask[i]]
    l = labels[labels["train"]]

    # Initializing the model
    et = l["total_energy"].values
    model = init_model_from_data(s, et)
    y = model.transformation.transform(et, s)

    # CV split to test and train folds
    cv_tr_indices = []
    cv_te_indices = []
    all_tr_indices = np.arange(np.sum(tr_mask))
    for split in range(5):
        cv_tr_indices.append(all_tr_indices[l["cv_split"] != split])
        cv_te_indices.append(all_tr_indices[l["cv_split"] == split])
    cv_splits = list(zip(cv_tr_indices, cv_te_indices))

    sigma_boss_iterarion = 1

    print("Initiating optimization")

    # Optimize sigma with BOSS
    boss_run_sigma = sigma_opt_boss()

    # Optimize KRR parameters for the predicted minimum sigma
    sigma_pred_log = boss_run_sigma.xmin[0]
    sigma_pred = 10**sigma_pred_log
    print(
        "\nFinal KRR optimization: log_sigma = {} ({})".format(
            sigma_pred_log, sigma_pred
        )
    )
    boss_run_krr = gamma_opt_boss(sigma_pred, "gamma_boss_final")

    fmin = boss_run_krr.fmin
    fmin_etot = 10**fmin

    ybest = boss_run_krr.ybest
    ybest_etot = 10**ybest

    print(
        "Best prediction: {} {} {}".format(boss_run_krr.xmin, fmin, fmin_etot),
        flush=True,
    )
    print(
        "Best acquicition: {} {} {}".format(boss_run_krr.xbest, ybest, ybest_etot),
        flush=True,
    )

    print("\nOptimization finished")
    print("Predicted minimum:")
    print("  sigma = {} (log10 sigma = {})".format(sigma_pred, sigma_pred_log))
    print(
        "  gamma = {} (log10 gamma = {})".format(
            10 ** boss_run_krr.xmin[0], boss_run_krr.xmin[0]
        )
    )
    print("  MAE = {} ({} eV)".format(fmin, fmin_etot))
