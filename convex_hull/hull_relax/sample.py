import sys
import os
import numpy as np
from multiprocessing import Pool
from itertools import combinations

from ase.io import write

sys.path.insert(0, "../../code/")
from load_model import *
from generate_structure import *
from relax_structure import *


model = load_model_uniform_data("../../models/")


def label_to_structure(template, label):
    """
    Transforms a binary Cl/Br label into an atomic structure
    Args:
        template (ASE Atoms object): atomic geometry that determines the atomic positions and lattice parameters of the returned structure
        label (string): binary label ('101101...') of length 24 that determines which X-site atoms are Cl or Br (0=Br, 1=Cl)
    Returns:
        ASE Atoms object: atomic structure
    """
    s = copy.deepcopy(template)
    e = s.get_atomic_numbers()
    numbers = [17 if i == "1" else 35 for i in label]
    e[8 : 8 + 24] = numbers
    s.set_atomic_numbers(e)
    return s


def structure_to_label(structure):
    """
    Determines the binary Cl/Br label of an atomic structure
    Args:
        structure (ASE Atoms object): atomic structure
    Returns:
        string: binary label ('101101...') of length 24 based on which X-site atoms are Cl or Br (0=Br, 1=Cl)
    """
    numbers = structure.get_atomic_numbers()[8 : 8 + 24]
    return "".join("1" if i == 17 else "0" for i in numbers)


def find_e(perm, template):
    """
    Uses the ML model to relax the atomic structure and predict the total
    energy related to a permutation of Cl/Br atoms.
    Args:
        perm (ndarray): binary array of length 24 that determines which X-site atoms are Cl or Br ('0'=Br, '1'=Cl)
        template (ASE Atoms object): atomic structure that determines the atomic positions and cell
    Returns:
        float: predicted total energy
    """
    label = "".join(perm)
    s_init = label_to_structure(template, label)
    s_relax, calc = relax_structure(s_init, model, relax_cell=False)
    return label, s_init, s_relax, calc


def sample_mc(output_file, N_cl, rng, N_samples=200, T_initial=10, T_final=1e-6):
    """
    Samples atomic CsPb(Br/Cl)3 structures using Monte Carlo simulated
    annealing. The simulated temperature is decreased linearly.
    Args:
        output_file (string): path to the output file
        N_cl (string): Number of Cl atoms
        N_samples (int): Number of sampled structures
        T_initial (float): Initial temperature in K
        T_final (float): Final temperature in K
    Returns:
        list: Sampled atomic structures pre-relaxation
        list: Sampled atomic structures after ML relaxation
    """
    template = generate_cspbclbr_structure("pnma", N_cl, "../../data/")

    # Randomize initial permutation
    perm_prev = np.array(N_cl * ["1"] + (24 - N_cl) * ["0"])
    rng.shuffle(perm_prev)

    label, s_init, s_relax, calc = find_e(perm_prev, template)
    e_prev = calc.e[-1]

    s_i = [s_init]
    s_r = [s_relax]

    k = 8.617333262145e-5  # Boltzmann constant in eV/K
    T = np.linspace(T_initial, T_final, N_samples - 1)

    with open(output_file, "w") as f:
        print(0, label, calc.e[0], calc.e[-1], len(calc.e), 1, file=f, flush=True)
        for i, t in enumerate(T):
            # Take a MC step by swapping random X-site atoms
            i1 = rng.choice(np.arange(24)[perm_prev == "0"], 1)[0]
            i2 = rng.choice(np.arange(24)[perm_prev == "1"], 1)[0]
            perm_new = copy.deepcopy(perm_prev)
            perm_new[i1], perm_new[i2] = perm_new[i2], perm_new[i1]

            label, s_init, s_relax, calc = find_e(perm_new, template)
            e_new = calc.e[-1]
            s_i.append(s_init)
            s_r.append(s_relax)

            de = e_new - e_prev
            accept = de < 0

            if not accept:
                p = np.exp(-de / (k * t))
                r = rng.uniform(0, 1, 1)[0]
                accept = r < p

            if accept:
                perm_prev = perm_new
                e_prev = e_new

            print(
                i + 1,
                label,
                calc.e[0],
                calc.e[-1],
                len(calc.e),
                int(accept),
                file=f,
                flush=True,
            )

    return s_i, s_r


def sample_all(output_file, N_cl):
    """
    Samples all atomic CsPb(Br/Cl)3 structures with the given number of Cl
    atoms.
    Args:
        output_file (string): path to the output file
        N_cl (string): Number of Cl atoms
    Returns:
        list: Sampled atomic structures pre-relaxation
        list: Sampled atomic structures after ML relaxation
    """
    template = generate_cspbclbr_structure("pnma", N_cl, "../../data/")
    perms = []
    for positions in combinations(np.arange(24), N_cl):
        perm = np.array(24 * ["0"])
        for i in positions:
            perm[i] = "1"
        perms.append(perm)

    s_i = []
    s_r = []

    with open(output_file, "w") as f:
        for i, p in enumerate(perms):
            label, s_init, s_relax, calc = find_e(p, template)

            s_i.append(s_init)
            s_r.append(s_relax)

            print(i, label, calc.e[0], calc.e[-1], len(calc.e), 0, file=f, flush=True)

    return s_i, s_r


def sample_hull(seed):
    """
    Samples the convex hull for CsPb(Br/Cl)3. When the number of Cl atoms is
    smaller than 3 or larger than 21, all possible configurations are sampled.
    Otherwise, 200 configurations are sampled using MC simulated annealing.
    Args:
        seed (int): Seed for the random number generation in MC
    """
    rng = np.random.RandomState(seed)

    dir_path = "samples/seed_{}".format(seed)
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

    for N_cl in range(25):
        output_file = "{}/samples_{:0>2}.out".format(dir_path, N_cl)
        s_init_file = "{}/structures_init_{:0>2}.xyz".format(dir_path, N_cl)
        s_relax_file = "{}/structures_relax_{:0>2}.xyz".format(dir_path, N_cl)

        if N_cl < 3 or N_cl > 21:
            s_i, s_r = sample_all(output_file, N_cl)
        else:
            s_i, s_r = sample_mc(output_file, N_cl, rng)

        write(s_init_file, s_i)
        write(s_relax_file, s_r)


if __name__ == "__main__":
    # Repeat the convex hull sampling with 5 different seeds
    seeds = [0, 1, 2, 3, 4]
    with Pool(5) as p:
        p.map(sample_hull, seeds)
