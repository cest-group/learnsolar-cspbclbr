import sys
import os
import numpy as np

from ase.io import read, write

sys.path.insert(0, "../../code/")
from init_mbtr import *


def find_duplicates(X,limit=1e-5):
    """
    Finds symmetry equivalent atomic structures based on their MBTR vectors.
    Args:
        X (ndarray): MBTR vectors
        limit (float): If the Euclidean distance between two MBTR vectors is smaller than the limit, the structures are considered symmetry equivalent.
    Returns:
        list: symmetry equivalent groups
    """
    duplicates = []
    for i,x in enumerate(X):
        is_duplicate = False
        for j,dup_i in enumerate([dup[0] for dup in duplicates]):
            x_dup = X[dup_i]
            d = np.linalg.norm(x_dup-x)
            if d<limit:
                duplicates[j].append(i)
                is_duplicate = True
                break
        if not is_duplicate:
            duplicates.append([i])
    return duplicates

def extract_symmetry_inequivalent_structures():
    """
    Extracts the symmetry inequivalent structures from the sampled structures.
    """
    os.mkdir("individual_samples")

    # Using a higher tier MBTR descriptor to make sure that all symmetric
    # structures are detected
    mbtr = tier_sigma_n_mbtr(3,1e-2,50,"l2_each")

    individual_e_init_all = []
    individual_e_relax_all = []
    individual_s_all = []
    individual_s_relax_all = []
    for N_cl in range(25):
        s = []
        s_relax = []
        e_i = []
        e_r = []
        for seed in range(5):
            path = "samples/seed_{}/samples_{:0>2}.out".format(seed, N_cl)
            i, label, e_init, e_relax, l = np.loadtxt(path, dtype=str).T
            e_init = e_init.astype(float)
            e_relax = e_relax.astype(float)
            
            if type(e_init)==np.float64: e_init = np.array([e_init])
            if type(e_relax)==np.float64: e_relax = np.array([e_relax])
            
            e_i+= list(e_init)
            e_r += list(e_relax)
            s += read("samples/seed_{}/structures_init_{:0>2}.xyz".format(seed, N_cl), index=':')
            s_relax += read("samples/seed_{}/structures_relax_{:0>2}.xyz".format(seed, N_cl), index=':')
        
        e_i = np.array(e_i)
        e_r = np.array(e_r)
        sort = e_r.argsort()
        e_i = e_i[sort]
        e_r = e_r[sort]
        s = [s[i] for i in sort]
        s_relax = [s_relax[i] for i in sort]

        X = mbtr.create(s,n_jobs=4)
        
        # Finding the symmetric structures and picking the one with median energy
        duplicates = find_duplicates(X)
        individual_i = np.array([dup[len(dup)//2] for dup in duplicates])
        individual_x = X[individual_i]
        individual_e_i = e_i[individual_i]
        individual_e_r = e_r[individual_i]
        individual_s = [s[i] for i in individual_i]
        individual_s_relax = [s_relax[i] for i in individual_i]
        
        print(N_cl, len(s), len(individual_s))

        individual_e_init_all += list(individual_e_i)
        individual_e_relax_all += list(individual_e_r)
        individual_s_all += individual_s
        individual_s_relax_all += individual_s_relax
        
    # Saving the symmetry inequivalent structures to individual_samples/
    write("individual_samples/structures_init.xyz", individual_s_all)
    write("individual_samples/structures_ml_relax.xyz", individual_s_relax_all)
    np.savetxt("individual_samples/e_init.dat", individual_e_init_all)
    np.savetxt("individual_samples/e_ml_ml.dat", individual_e_relax_all)

def extract_minimum_structures():
    """
    Extracts the minimum energy structures from the symmetry inequivalent
    structures.
    """
    minimum_structures_init = []
    minimum_structures_relax = []
    minimum_e_init = []
    minimum_e_relax = []

    s_init = read("individual_samples/structures_init.xyz", index=':')
    s_relax = read("individual_samples/structures_ml_relax.xyz", index=':')
    e_init = np.loadtxt("individual_samples/e_init.dat")
    e_ml_ml = np.loadtxt("individual_samples/e_ml_ml.dat")
    n_cl = np.array([np.sum(st.get_atomic_numbers() == 17) for st in s_init])

    # Finding the minimum energy structure for each Cl concentration level.
    for N_cl in range(25):
        mask = (n_cl==N_cl)

        s_init_cl = [s for i,s in enumerate(s_init) if mask[i]]
        s_relax_cl = [s for i,s in enumerate(s_relax) if mask[i]]
        e_init_cl = e_init[mask]
        e_relax_cl = e_ml_ml[mask]
        
        argmin = e_relax_cl.argmin()
        minimum_e_init.append(e_init_cl[argmin])
        minimum_e_relax.append(e_relax_cl[argmin])
        minimum_structures_init.append(s_init_cl[argmin])
        minimum_structures_relax.append(s_relax_cl[argmin])

    # Saving the minimum energy structures to minimum_structures/
    path = "minimum_structures"
    os.mkdir(path)
    np.savetxt(f"{path}/e_init.dat", minimum_e_init)
    np.savetxt(f"{path}/e_ml_ml.dat", minimum_e_relax)
    write(f"{path}/structures_init.xyz", minimum_structures_init)
    write(f"{path}/structures_ml_relax.xyz", minimum_structures_relax)

if __name__=="__main__":
    extract_symmetry_inequivalent_structures()
    extract_minimum_structures()
