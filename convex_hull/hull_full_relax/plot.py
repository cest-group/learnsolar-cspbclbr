import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms

from ase.io import read

sys.path.insert(0, "../../code/")
from energy_transformation import *
from load_model import *

# Energy transformation for plotting
plot_trans = lambda e, s: 1000 / 8 * EnthalpyOfMixingTransformation().transform(e, s)

model = load_model_uniform_data("../../models/")

# Setting up MBTR representation with normalization for distance calculations
mbtr = MBTR(
    species=model.mbtr.species,
    periodic=model.mbtr.periodic,
    k1=model.mbtr.k1,
    k2=model.mbtr.k2,
    k3=model.mbtr.k3,
    normalize_gaussians=model.mbtr.normalize_gaussians,
    normalization="l2_each",
    flatten=True,
    sparse=model.mbtr.sparse,
)

s_samples = read("individual_samples/structures_ml_relax.xyz", index=":")
e_samples = np.loadtxt("individual_samples/e_ml_ml.dat")
n_cl = np.array([np.sum(s.get_atomic_numbers() == 17) for s in s_samples])

mbtrs = mbtr.create(s_samples, n_jobs=4)
dists = np.zeros(len(s_samples))
for N_cl in range(25):
    mask = n_cl == N_cl
    e_cl = e_samples[mask]
    mbtr_cl = mbtrs[mask]
    mbtr_min = mbtr_cl[e_cl.argmin()]
    dists[mask] = np.array([np.linalg.norm(mbtr_min - m) for m in mbtr_cl])

x = n_cl / 24
x_plot = np.arange(25) / 24

fig, axs = plt.subplots(2, sharex=True)

# plot samples
e_plot = plot_trans(e_samples, s_samples)
cm = plt.cm.get_cmap("jet")
sc = axs[0].scatter(x, e_plot, c=dists, cmap=cm)
cb = fig.colorbar(sc, ax=axs[0], orientation="horizontal", location="top")

# ML relax
s = read("minimum_structures/structures_ml_relax.xyz", index=":")
e_ml_ml = np.loadtxt("minimum_structures/e_ml_ml.dat")
e_ml_ml_plot = plot_trans(e_ml_ml, s)
axs[1].plot(
    x_plot, e_ml_ml_plot, "-o", c="tab:blue", label="ML opt. $\Delta H_{mix}^{ML}$"
)

# ML relax, DFT energy
s = read("minimum_structures/structures_ml_relax.xyz", index=":")
e_ml_dft = np.loadtxt("minimum_structures/e_ml_dft.dat")
e_ml_dft_plot = plot_trans(e_ml_dft, s)
axs[1].plot(
    x_plot,
    e_ml_dft_plot,
    "-o",
    c="tab:orange",
    label="ML opt. $\Delta H_{\mathrm{mix}}^{\mathrm{DFT}}$",
)
print(
    f"ML relax, DFT energy MAE: {np.mean(np.abs(e_ml_ml_plot-e_ml_dft_plot))} meV/f.u."
)


# DFT relax, DFT energy
s = read("minimum_structures/structures_ml_relax.xyz", index=":")
e_dft_dft = np.loadtxt("minimum_structures/e_dft_dft.dat")
e_dft_dft_plot = plot_trans(e_dft_dft, s)
axs[1].plot(
    x_plot,
    e_dft_dft_plot,
    "-s",
    c="tab:green",
    label="DFT opt. $\Delta H_{\mathrm{mix}}^{\mathrm{DFT}}$",
)
print(
    f"DFT relax, DFT energy MAE: {np.mean(np.abs(e_ml_ml_plot-e_dft_dft_plot))} meV/f.u."
)

# convex hull
axs[1].plot(x_plot[[0, 8, 24]], e_dft_dft_plot[[0, 8, 24]], "k-", label="Convex hull")

plt.xlabel("Cl concentration")
axs[0].set_ylabel("$\Delta H_{\mathrm{mix}}$ (meV/f.u.)")
axs[1].set_ylabel("$\Delta H_{\mathrm{mix}}$ (meV/f.u.)")

axs[1].legend(loc="center right")

plt.tight_layout()
plt.show()
