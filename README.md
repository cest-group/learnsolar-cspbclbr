# README

## LearnSolar
LearnSolar is a research project where machine learning (ML) is used in the study of perovskite solar cell materials. We develop ML models for material property prediction and apply them in solving perovskite design problems through compositional engineering. In this repository, we compute the convex hull for CsPb(Cl/Br)3 inorganic perovskite alloy. The related article is available at https://doi.org/10.1103/PhysRevMaterials.6.113801.

## Workflow

### Data Sets

This repository contains procesessed data sets of 40 atom CsPb(Cl/Br)3 atomic structures and their DFT total energies. The same processed data is available through Zenodo (https://doi.org/10.5281/zenodo.7153019) and the raw DFT calculation data through NOMAD (https://doi.org/10.17172/NOMAD/2022.10.06-1).

The data sets are located in `data/` and they can be loaded using the functions in `code/load_data.py` . List of data sets:

| Data set name | Information |
| ------------- | ----------- |
| **cspbclbr\_single\_point** | Algorithmically generated CsPb(Cl/Br)3 structures of four different space groups: Pm-3m, P4/mbm, I4/mcm, and Pnma. The Cl concentration follows binomial distribution. |
| **cspbclbr\_single\_point\_tails** | Extends on **cspbclbr\_single\_point** by adding structures at the ends of the Cl concentration range. |
| **cspbclbr\_relax\_uniform** | Structure snapshots from 200 DFT relaxations. Covers equally phases Pm-3m, P4/mbm, I4/mcm, and Pnma. The Cl concentration range is covered uniformly. Has atomic forces for each structure. |
|**cspbclbr_end_points**| DFT relaxed pure CsPbCl3 and CsPbBr3 structures. Four different space groups: Pm-3m, P4/mbm, I4/mcm, and Pnma. |

The data sets are also combined to create new data sets in `code/merge_data.py`. List of merged data sets:

| Data set name | Information |
| ------------- | ----------- |
| **merged\_uniform\_single\_point** | Combination of **cspbclbr\_single\_point** and **cspbclbr\_single\_point\_tails**. 10 000 structures picked randomly from the two sets so that the Cl concentration follows uniform distribution over the whole range [0,1].|
| **merged\_uniform** | Union of **merged\_uniform\_single\_point** and **cspbclbr\_relax\_uniform**.|

### ML Model
We use many-body tensor representation (MBTR) from [DScribe](https://singroup.github.io/dscribe/latest/) to represent the atomic structures in vector form. The MBTR vectors are mapped to energy values with kernel ridge regression (KRR) from [scikit-learn](https://scikit-learn.org/). The model is handled by `Model` class in `code/model.py`.


### Hyperparameter Optimization
The hyperparameters of the ML model need to be optimized to reach the full potential of the model. In directory `hp_opt/uniform_data/` we optimize MBTR parameter sigma and KRR parameter gamma with Bayesian optimization code [BOSS](https://cest-group.gitlab.io/boss/). 80% of data set **merged\_uniform** was used to cross-validate the ML model's energy prediction error. A two-level approach was adopted for optimizing the hyperparameters. BOSS was used to sample sigma values and for each value another BOSS run was used to optimize gamma. The optimization was run with
```
$ cd hp_opt/uniform_data
$ python run.py | tee run.log
```
The results can be visualized by running
```
$ python run_postprocessing.py
```
in the same directory.

### Saving and Loading a ML Model
After optimizing the hyperparameters, a model initialization function with the optimal hyperparameters has been added to `code/model.py`. The model can be saved to a file by running
```
$ cd code
$ python save_model.py
```
The model binary appears in `models/`.

The saved model can be loaded using the function in `code/load_model.py`.


### ML Model Testing
The performance of the ML models can be evaluated with the functions in `code/test_model.py`:
```
$ cd code
$ python test_model.py
```

### Convex Hull Sampling
The trained ML model has been applied in computing the convex hull of CsPb(Cl/Br)3. The code can be run through
```
$ cd convex_hull/hull_full_relax
$ python sample.py
$ python extract_minima.py
```
The results can be plotted by running
```
$ python plot.py
```
in the same directory.
