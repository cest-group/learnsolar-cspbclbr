import numpy as np

from dscribe.descriptors import MBTR


def r_sigma_n_mbtr(
    mbtr_k2_r_cut, mbtr_k2_grid_sigma, mbtr_k2_grid_n, normalization="none"
):
    """
    Defines an MBTR representation with (r_cut,sigma,n) parameters
    Args:
        mbtr_k2_r_cut (float): cutoff radius of the representation
        mbtr_k2_grid_sigma (float): width of the gaussian distributions
        mbtr_k2_grid_n (int): number of grid points per element pair
        normalization (string): MBTR normalization
    Returns:
        MBTR object: Dscribe MBTR descriptor
    """

    mbtr_k2_grid_min = -0.1
    mbtr_k2_grid_max = 0.6
    mbtr_k2_weight_cutoff = 1e-3

    mbtr = MBTR(
        species=["Cs", "Pb", "Cl", "Br"],
        k2={
            "geometry": {"function": "inverse_distance"},
            "grid": {
                "min": mbtr_k2_grid_min,
                "max": mbtr_k2_grid_max,
                "sigma": mbtr_k2_grid_sigma,
                "n": mbtr_k2_grid_n,
            },
            "weighting": {
                "function": "exp",
                "r_cut": mbtr_k2_r_cut,
                "threshold": mbtr_k2_weight_cutoff,
            },
        },
        periodic=True,
        flatten=True,
        normalization=normalization,
    )

    return mbtr


def tier_sigma_n_mbtr(tier, mbtr_k2_grid_sigma, mbtr_k2_grid_n, normalization="none"):
    """
    Defines an MBTR representatio with (tier,sigma,n) parameters
    Args:
        tier (int): Tier of the representation. Tiers 1, 2, and 3 correspond to cutoff radii 6.27, 9.405, and 12.54 angstroms, respectfully
        mbtr_k2_grid_sigma (float): width of the gaussian distributions
        mbtr_k2_grid_n (int): number of grid points per element pair
        normalization (string): MBTR normalization
    Returns:
        Dscribe MBTR object: MBTR descriptor
    """

    assert tier in [1, 2, 3]

    if tier == 1:
        r_cutoff = 5.7 * 1.1  # 1.0*1.1 times mean lattice vector length
    elif tier == 2:
        r_cutoff = 1.5 * 5.7 * 1.1  # 1.5*1.1 times mean lattice vector length
    elif tier == 3:
        r_cutoff = 2 * 5.7 * 1.1  # 2.0*1.1 times mean lattice vector length

    return r_sigma_n_mbtr(r_cutoff, mbtr_k2_grid_sigma, mbtr_k2_grid_n, normalization)
