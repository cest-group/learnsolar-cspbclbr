import sys
import numpy as np
import matplotlib.pyplot as plt
import copy

from ase import Atoms
import ase.io


def generate_cspbclbr_structure(phase, n_cl, data_path="../data/", shuffle_rng=None):
    """
    Generates a 2x2x2 supercell CsPb(Cl,Br)3 perovskite geometry by interpolating linearly from the end point structures CsPbBr3 and CsPbCl3.
    Args:
        phase (string): lattice type of the structure ("pm3m", "pnma", "i4mcm", or "p4mbm")
        n_cl (int): number of Cl atoms. Has to be in range [0,24]
        data_path (string): relative path to the data folder
        shuffle_rng (numpy RandomState): RandomState for shuffling the Cl and Br atoms. If 'None', the atoms are not shuffled.
    Returns:
        ASE Atoms object: generated structure
    """
    phase = phase.lower()

    assert phase in ["pm3m", "pnma", "i4mcm", "p4mbm"]
    assert n_cl >= 0 and n_cl <= 24

    cell = {}
    frac = {}
    for element in ["br", "cl"]:
        path = f"{data_path}/cspbclbr_end_points/cspb{element}3_{phase}/geometry.in"
        cell[element] = np.genfromtxt(
            path, usecols=[1, 2, 3], skip_footer=40, dtype=float
        )
        frac[element] = np.genfromtxt(
            path, usecols=[1, 2, 3], skip_header=4, dtype=float
        )

    z = 1.0 - n_cl / 24.0

    cell = z * cell["br"] + (1.0 - z) * cell["cl"]
    frac = z * frac["br"] + (1.0 - z) * frac["cl"]

    e = np.array(8 * [82] + n_cl * [17] + (24 - n_cl) * [35] + 8 * [55])

    if shuffle_rng:
        shuffle_rng.shuffle(e[8 : 8 + 24])

    return Atoms(scaled_positions=frac, cell=cell, numbers=e, pbc=True)
