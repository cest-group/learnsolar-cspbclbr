from ase.calculators.calculator import Calculator

from model import *


class ML_Calculator(Calculator):
    """
    ASE Calculator that can be used to predict total energies, atomic forces,
    and stresses with the ML model.
    """

    implemented_properties = ["energy", "forces", "stress"]

    def __init__(self, model):
        self.model = model

        self.max_f = []
        self.max_fi = []
        self.e = []

        super().__init__()

    def calculate(self, atoms, properties, system_changes):
        super().calculate(atoms, properties, system_changes)

        # Always calculate all three properties. While using the calculator
        # for structure optimization this turns out to be the easiest way to
        # avoid unnecessary recalculation of MBTR vectors.
        prop = ["y", "atomic_derivatives", "strain_derivatives"]
        pred = self.model.predict_from_structure([atoms], properties=prop, n_jobs=1)

        e = pred["y"]
        self.e.append(e[0])
        self.results["energy"] = e

        f = -pred["atomic_derivatives"].reshape(-1, 3)
        self.max_fi.append(np.max(f))
        self.max_f.append(np.max(np.sum(f**2, axis=1) ** 0.5))
        self.results["forces"] = f

        stress = pred["strain_derivatives"].reshape((6)) / atoms.get_volume()
        self.results["stress"] = stress
