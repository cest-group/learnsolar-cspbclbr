import pickle
from model import *


def load_model_uniform_data(models_path="../models/"):
    """
    Loads the fitted ML model.
    Args:
        models_path (string): relative path to the models directory
    Returns:
        Model object: fitted ML model
    """
    model_file = "{}/uniform_data.bin".format(models_path)

    model = pickle.load(open(model_file, "rb"))

    return model
