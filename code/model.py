import numpy as np

from dscribe.descriptors import MBTR

from krr_d import KRR_d
from mbtr_d import MBTR_d
from energy_transformation import *


class Model:
    """
    Class that implements the ML model. Encapsulates the MBTR and KRR
    components of the model as well as an energy transformation for label
    preprocessing.
    """

    def __init__(self, mbtr, krr, transformation=IdentityTransformation()):
        """
        Constructs a Model object.
        Args:
        mbtr (Dscribe MBTR object): MBTR descriptor
        krr (KRR_d object): KRR model
        transformation (EnergyTransformation object): Energy transformation
        """
        self.mbtr = mbtr
        self.krr = krr
        self.transformation = transformation

    def fit_X(self, X, y, structures):
        """
        Fits the model on the MBTR vectors X and property y.
        Args:
            X (ndarray): MBTR vectors
            y (ndarray): fitted property
            structures (list): list of ASE Atoms objects. Required for the energy transformation.
        """
        y_trans = self.transformation.transform(y, structures)
        self.krr.fit(X, y_trans)

    def fit(self, structures, y, n_jobs=4):
        """
        Computes the MBTR vectors of given structures and fits the model on
        property y.
        Args:
            structures (list): list of ASE Atoms objects
            y (ndarray): fitted property
            n_jobs: number of tasks used for creating the MBTR vectors.
        Returns:
            list: list of MBTR vectors
        """
        X = self.mbtr.create(structures, n_jobs=n_jobs)
        self.fit_X(X, y, structures)
        return X

    def predict_from_X(self, X, structures):
        """
        Predicts property y based on MBTR vectors X.
        Args:
            X (ndarray): MBTR vectors
            structures (list): list of ASE Atoms objects. Required for the energy transformation.
        Returns:
            ndarray: predicted properties y
        """
        y_trans = self.krr.predict(X)
        y = self.transformation.inv_transform(y_trans, structures)
        return y

    def predict_d_from_X(self, X, X_d):
        """
        Predicts derivatives of property y based on MBTR vectors X and its
        derivatives.
        Args:
            X (ndarray): MBTR vectors
            X_d (ndarray): derivatives of X
        Returns:
            ndarray: predicted derivatives of property y
        """
        y_trans_d = self.krr.predict_d(X, X_d)
        y_d = y_trans_d / self.transformation.scale()
        return y_d

    def predict_from_structure(self, structures, properties=["y"], n_jobs=4):
        """
        Predicts property y and/or its derivatives of property y from atomic
        structure.
        Args:
            structures (list): list of ASE Atoms objects
            properties (list): list of properties to predict ("y", "atomic_derivatives", "strain_derivatives", and "X")
            X_d (ndarray): derivatives of X
            n_jobs: number of tasks
        Returns:
            ndarray or dictionary: predicted properties. If the length of 'properties' is 1, returns an array. Otherwise, returns a dictionary.
        """
        for p in properties:
            if p not in ["y", "atomic_derivatives", "strain_derivatives", "X"]:
                raise ValueError(f"'{p}' is not a supported property.")

        n_atoms = len(structures[0].get_atomic_numbers())

        ret = {}
        if "y" in properties:
            ret["y"] = np.zeros(len(structures))
        if "atomic_derivatives" in properties:
            ret["atomic_derivatives"] = np.zeros((len(structures), n_atoms, 3))
        if "strain_derivatives" in properties:
            ret["strain_derivatives"] = np.zeros((len(structures), 6))
        if "X" in properties:
            ret["X"] = np.zeros((len(structures), self.mbtr.get_number_of_features()))

        if ("atomic_derivatives" not in properties) and (
            "strain_derivatives" not in properties
        ):
            X = self.mbtr.create(structures, n_jobs=n_jobs)
            if "y" in ret:
                ret["y"] = self.predict_from_X(X, structures)
            if "X" in ret:
                ret["X"] = X

        else:
            mbtr_d = MBTR_d(self.mbtr, atoms_per_system=n_atoms)
            # Iterating over the structures n_jobs at a time in order to use less memory
            for start in range(0, len(structures), n_jobs):
                end = min(start + n_jobs, len(structures))
                s_part = [structures[i] for i in range(start, end)]

                X_d, X = mbtr_d.derivatives(
                    s_part,
                    return_atomic_derivatives=("atomic_derivatives" in properties),
                    return_strain_derivatives=("strain_derivatives" in properties),
                    n_jobs=n_jobs,
                )

                # Reshaping
                X = X.reshape(len(s_part), -1)
                X_d = X_d.reshape((len(s_part), -1, X.shape[1]))

                # Casting to float64 because of a bug in Dscribe that causes
                # the return type to be different when there is only one
                # structure (float32) instead of many (float64). The different
                # type causes problems with njit in KRR_d.predict_d().
                if X.dtype != np.float64:
                    X = X.astype(np.float64)
                if X_d.dtype != np.float64:
                    X_d = X_d.astype(np.float64)

                d = self.predict_d_from_X(X, X_d)

                if "y" in ret:
                    ret["y"][start:end] = self.predict_from_X(X, s_part)
                if "atomic_derivatives" in ret:
                    ret["atomic_derivatives"][start:end] = d[:, : n_atoms * 3].reshape(
                        -1, n_atoms, 3
                    )
                if "strain_derivatives" in ret:
                    ret["strain_derivatives"][start:end] = d[:, -6:]
                if "X" in ret:
                    ret["X"][start:end] = X

        return ret[properties[0]] if len(properties) == 1 else ret


def init_model_from_data(structures, e_tot):
    """
    Initializes a model, where the label transformations are determined based
    on the given data set. Hyperparameters are not optimized.
    Args:
        structures (list): list of ASE Atoms objects
        e_tot (ndarray): total energies of the structures
    Returns:
        Model object: ML model with the desired label transformation
    """
    from init_mbtr import tier_sigma_n_mbtr

    transformation = PreprocessingTransformation(e_tot, structures)

    # Initial guess for MBTR (hyperparameters not optimized)
    mbtr = tier_sigma_n_mbtr(1, 2e-2, 50)

    # Initial guess for KRR (hyperparameters not optimized)
    krr = KRR_d(kernel="rbf", alpha=1e-3, gamma=1e-2)

    return Model(mbtr, krr, transformation)


def optimized_uniform_data_model(data_path="../data/", fit=False):
    """
    Initializes a model based on the merged uniform data set. The
    hyperparameters have been optimized.
    Args:
        data_path (string): relative path to the data directory
        fit (boolean): If 'True', fits the model to the training portion of the data set.
    Returns:
        Model object: ML model with optimized hyperparameters.
    """
    from merge_data import merged_uniform_data
    from init_mbtr import tier_sigma_n_mbtr

    s, l = merged_uniform_data(data_path)

    model = init_model_from_data(s, l["total_energy"])

    # Optimized hyperparameters (../hp_opt/uniform_data_norm_none)
    sigma = 0.017519115236635203
    alpha = 1e-5
    gamma = 0.00012801248464134623

    model.mbtr = tier_sigma_n_mbtr(1, sigma, 50)
    model.krr = KRR_d(kernel="rbf", alpha=alpha, gamma=gamma)

    if fit:
        s_tr = [s[i] for i, train in enumerate(l["train"]) if train]
        et_tr = l.loc[l["train"], "total_energy"]
        model.fit(s_tr, et_tr)

    return model
