import numpy as np
import copy
from abc import ABC, abstractmethod


# DFT energies for atoms and small molecules
_dft_energies = {
    "cs": -215280.90465796,
    "sn": -170287.10405185,
    "pb": -589995.99249880,
    "cl": -12558.77053708,
    "br": -71354.09889926,
    "i": -196508.80427548,
    "rb": -81679.30914873,
    "cscl": -227846.071593067,
    "csbr": -286640.972017458,
    "pbcl2": -615123.56955639,
    "pbbr2": -732713.322577515,
    "cspbbr3_pnma": -8154836.843918324,
    "cspbcl3_pnma": -6743759.909496200,
}


class EnergyTransformation(ABC):
    """Abstract class for energy transformations"""

    @abstractmethod
    def transform(self, e_tot, structures):
        """
        Transforms total energy to a transformed quantity.
        Args:
            e_tot (ndarray): total energies
            structures (list): list of ASE atoms objects
        Returns:
            ndarray: transformed energies
        """

    @abstractmethod
    def inv_transform(self, e_trans, structures):
        """
        Transforms transformed energies back to total energy.
        Args:
            e_trans (ndarray): transformed energies
            structures (list): list of ASE atoms objects
        Returns:
            ndarray: total energies
        """

    @abstractmethod
    def scale(self):
        """Scaling factor between total energy and transformed quantity
        differences"""


class IdentityTransformation(EnergyTransformation):
    """Energy transformation class for which the transformed energy is the total energy"""

    def transform(self, e_tot, structures):
        return e_tot

    def inv_transform(self, e_trans, structures):
        return e_trans

    def scale(self):
        return 1


class EnthalpyOfMixingTransformation(EnergyTransformation):
    """Energy transformation class that transforms total energy of CsPb(Cl/Br)3 structures to enthalpy of mixing."""

    def transform(self, e_tot, structures):
        e0 = _dft_energies["cspbbr3_pnma"]
        e24 = _dft_energies["cspbcl3_pnma"]

        n_cl = np.array([np.sum(s.get_atomic_numbers() == 17) for s in structures])
        e_trans = e_tot - (e24 - e0) / 24 * n_cl - e0
        return e_trans

    def inv_transform(self, e_trans, structures):
        e0 = _dft_energies["cspbbr3_pnma"]
        e24 = _dft_energies["cspbcl3_pnma"]

        n_cl = np.array([np.sum(s.get_atomic_numbers() == 17) for s in structures])
        e_tot = e_trans + (e24 - e0) / 24 * n_cl + e0
        return e_tot

    def scale(self):
        return 1


class PreprocessingTransformation(EnergyTransformation):
    """
    Energy transformation that combines the enthalpy of mixing transformation
    with translating and scaling the data so that the mean is 0 and the
    standard deviation is 1.
    """

    def __init__(self, e_tot, structures):
        e_nonlinear = EnthalpyOfMixingTransformation().transform(e_tot, structures)
        mean = np.mean(e_nonlinear)
        std = np.std(e_nonlinear)

        self._shift = -mean
        self._scale = 1 / std

    def transform(self, e_tot, structures):
        e_nonlinear = EnthalpyOfMixingTransformation().transform(e_tot, structures)
        e_trans = (e_nonlinear + self._shift) * self._scale
        return e_trans

    def inv_transform(self, e_trans, structures):
        e_scaled_and_shifted = e_trans / self._scale - self._shift
        e_tot = EnthalpyOfMixingTransformation().inv_transform(
            e_scaled_and_shifted, structures
        )
        return e_tot

    def scale(self):
        return self._scale
