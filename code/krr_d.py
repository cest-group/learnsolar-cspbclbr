from sklearn.kernel_ridge import *

from numba import njit


@njit
def calculate_K_d_njit(X, X_d, K, gamma, X_fit):
    """
    Calculates the derivatives of the kernel matrix using numba.
    Args:
        X (ndarray): input vectors
        X_d (ndarray): gradients of X
        K (ndarray): kernel matrix
        gamma (float): hyperparameter gamma
        X_fit: training set vectors
    Returns:
        ndarray: 3D array containing the gradients of the kernel matrix
    """
    K_d = np.zeros((X_d.shape[1], K.shape[0], K.shape[1]))
    for k in range(len(X)):
        for j in range(len(X_fit)):
            dist = X[k] - X_fit[j]
            for n in range(X_d.shape[1]):
                K_d[n, k, j] = -2 * K[k, j] * gamma * np.dot(X_d[k, n], dist)
    return K_d


class KRR_d(KernelRidge):
    """Class that inherits from sklearn Kernelridge and implements gradient predictions"""

    def predict_d(self, X, X_d):
        """
        Predicts KRR derivatives.
        Args:
            X (ndarray): input vectors
            X_d (ndarray): gradients of X
        Returns:
            ndarray: predicted gradients for X
        """
        check_is_fitted(self)

        K = self._get_kernel(X, self.X_fit_)
        K_d = calculate_K_d_njit(X, X_d, K, self.gamma, self.X_fit_)
        return np.dot(K_d, self.dual_coef_).T
