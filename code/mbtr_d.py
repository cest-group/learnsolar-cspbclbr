from dscribe.descriptors import MBTR

import numpy as np
import sys
from scipy.sparse import lil_matrix, coo_matrix
from math import erf

from numba import njit, jit, types, vectorize
from numba.typed import Dict, List

from ase import Atoms

from dscribe.descriptors import MBTR
from dscribe.core import System
import dscribe.utils.geometry


@vectorize(["float64(float64)"], nopython=True)
def njit_erf(x):
    return erf(x)


float_array_1d = types.float64[::1]
float_array_3d = types.float64[:, :, ::1]
int_tuple = types.UniTuple(types.int64, 2)


@njit
def get_k2_maps(
    n_atoms_system,
    adj_mat,
    positions,
    geom_func,
    weight_func,
    scale,
    cutoff,
    n,
    x_min,
    x_max,
    sigma,
    cell_indices,
    Z,
    element_index,
    calculate_atomic_derivatives,
    calculate_strain_derivatives,
):
    """
    Generates MBTR_k2 map, as well as its atomic and strain derivatives using
    Numba.
    Args:
        n_atoms_system (int): number of atoms in the original cell
        adj_mat (ndarray): neighbor list
        positions (ndarray): atom positions
        geom_func (string): geometry function ('distance' or 'inverse_distance')
        weight_func (string): weighting function ('exp', 'exponential', or 'unity')
        scale (float): scaling factor for exponential weighting
        cutoff (float): cutoff radius
        n (int): number of grid points per element pair
        x_min (float): minimum value of the x grid
        x_max (float): maximum value of the x grid
        sigma (float): standard deviation of the gaussian distribution peaks
        cell_indices (ndarray): indicates the periodic image where each atom is
        Z (ndarray): element for each atom
        element_index (ndarray): element index for each atom
        calculate_atomic_derivatives (boolean): If 'True', compute atomic derivatives
        calculate_strain_derivatives (boolean): If 'True', compute strain derivatives
    Returns:
        dictionary: partial k=2 MBTR vectors for all element pairs
        dictionary: atomic position derivatives of the partial MBTR vectors
        dictionary: strain derivatives of the partial MBTR vectors
    """
    k2_map = {}
    k2_atomic_derivative_map = {}
    k2_strain_derivative_map = {}

    dx = (x_max - x_min) / (n - 1)
    x = x_min - dx / 2 + dx * np.arange(n + 1)
    n_atoms = len(Z)

    # Loop over all atoms in the system
    for i in range(n_atoms):
        # For each atom we loop only over the neighbours
        adj_list = adj_mat[i]
        adj_list = adj_list[adj_list >= 0]
        for j in adj_list:
            if j <= i:
                continue

            # Only consider pairs that have one atom in the original cell
            if i >= n_atoms_system and j >= n_atoms_system:
                continue

            d_vec = positions[i] - positions[j]
            dist = np.sum(d_vec**2) ** 0.5

            # dd_mat = np.outer(d_vec,d_vec)
            dd_mat = np.outer(d_vec, d_vec).flatten()[np.array([0, 4, 8, 5, 2, 1])]

            # Calculate geometry value
            if geom_func == "inverse_distance":
                g = 1 / dist
                atomic_derivative_g = -d_vec / dist**3
                strain_derivative_g = -dd_mat / dist**3
            elif geom_func == "distance":
                g = dist
                atomic_derivative_g = d_vec / dist
                strain_derivative_g = dd_mat / dist

            # Calculate weight value
            if weight_func == "exponential" or weight_func == "exp":
                w = np.exp(-scale * dist)
                atomic_derivative_w = -w * scale / dist * d_vec
                strain_derivative_w = -w * scale / dist * dd_mat
                if w < cutoff:
                    continue
            elif weight_func == "unity":
                w = 1.0
                atomic_derivative_w = 0 * d_vec
                strain_derivative_w = 0 * dd_mat

            # Calculate gaussian
            cdf = 1 / 2 * (1 + njit_erf((x - g) / (sigma * 2**0.5)))
            gauss = (cdf[1:] - cdf[:-1]) / dx

            if calculate_atomic_derivatives or calculate_strain_derivatives:
                # Calculate x*gaussian distribution
                x_gauss_cdf = g * cdf - sigma / (2 * np.pi) ** 0.5 * (
                    np.exp(-((g - x) ** 2) / (2 * sigma**2)) - 1
                )
                x_gauss = (x_gauss_cdf[1:] - x_gauss_cdf[:-1]) / dx

            # Weighted distribution
            wd = w * gauss

            if calculate_atomic_derivatives:
                # Gradient of the weighted distribution
                atomic_derivative_wd = (
                    -np.outer(atomic_derivative_g, wd * sigma ** (-2) * g)
                    + np.outer(atomic_derivative_g, w * x_gauss * sigma ** (-2))
                    + np.outer(atomic_derivative_w, gauss)
                )

            if calculate_strain_derivatives:
                # Strain derivative of the weighted distribution
                strain_derivative_wd = (
                    -np.outer(strain_derivative_g, wd * sigma ** (-2) * g)
                    + np.outer(strain_derivative_g, w * x_gauss * sigma ** (-2))
                    + np.outer(strain_derivative_w, gauss)
                )

            # Rescale if the atoms are in different periodic images
            if np.any(cell_indices[i] != cell_indices[j]):
                wd /= 2
                if calculate_strain_derivatives:
                    strain_derivative_wd /= 2

            # Get the index of the present elements in the final vector
            i_index = element_index[i]
            j_index = element_index[j]

            # Save information in the part where j_index >= i_index
            # key = tuple(sorted([i_index, j_index]))
            key = (i_index, j_index) if i_index < j_index else (j_index, i_index)

            # Sum wd into output
            if key in k2_map:
                k2_map[key] = k2_map[key] + wd
            else:
                k2_map[key] = wd
                if calculate_atomic_derivatives:
                    k2_atomic_derivative_map[key] = np.zeros((n_atoms_system, 3, n))
                if calculate_strain_derivatives:
                    k2_strain_derivative_map[key] = np.zeros((6, n))

            if calculate_atomic_derivatives:
                # Add derivative contribution to derivatives that it affects.
                # Derivatives are antisymmetric.
                for index, sign in zip([i, j], [1, -1]):
                    if index < n_atoms_system:
                        k2_atomic_derivative_map[key][index] = (
                            k2_atomic_derivative_map[key][index]
                            + sign * atomic_derivative_wd
                        )

            if calculate_strain_derivatives:
                k2_strain_derivative_map[key] += strain_derivative_wd

    return k2_map, k2_atomic_derivative_map, k2_strain_derivative_map


class MBTR_d(MBTR):
    """
    Class that inherits from MBTR and implements MBTR derivatives with regard
    to atomic positions and strain tensor components.
    """

    def __init__(self, mbtr, atoms_per_system):
        """
        Constucts an MBTR_d object based on the given MBTR object and the
        number of atoms in the atomic structures that it will be used on.
        Args:
            mbtr (DScribe MBTR object): MBTR representation
            atoms_per_system: number of atoms in the atomic structures
        """
        assert mbtr.k1 == None, "Derivatives not implemented for k1!"
        assert mbtr.k3 == None, "Derivatives not implemented for k3!"
        assert mbtr.flatten == True, "Derivatives implemented only for flatten=True!"
        assert (
            mbtr.normalization != "l2_each"
        ), "Derivatives not implemented for 'l2_each' normalization!"
        self.atoms_per_system = atoms_per_system
        super().__init__(
            species=mbtr.species,
            periodic=mbtr.periodic,
            k1=mbtr.k1,
            k2=mbtr.k2,
            k3=mbtr.k3,
            normalize_gaussians=mbtr.normalize_gaussians,
            normalization=mbtr.normalization,
            flatten=mbtr.flatten,
            sparse=mbtr.sparse,
        )

    def derivatives(
        self,
        system,
        return_descriptor=True,
        return_atomic_derivatives=True,
        return_strain_derivatives=False,
        n_jobs=1,
        only_physical_cores=False,
        verbose=False,
    ):
        """Return the descriptor derivatives for the given system.

        Args:
            system (:class:`ase.Atoms` or list of :class:`ase.Atoms`): One or
                many atomic structures.
            return_descriptor (bool): Whether to also calculate the descriptor
                in the same function call. Notice that it typically is faster
                to calculate both in one go.
            n_jobs (int): Number of parallel jobs to instantiate. Parallellizes
                the calculation across samples. Defaults to serial calculation
                with n_jobs=1. If a negative number is given, the number of jobs
                will be calculated with, n_cpus + n_jobs, where n_cpus is the
                amount of CPUs as reported by the OS. With only_physical_cores
                you can control which types of CPUs are counted in n_cpus.
            only_physical_cores (bool): If a negative n_jobs is given,
                determines which types of CPUs are used in calculating the
                number of jobs. If set to False (default), also virtual CPUs
                are counted.  If set to True, only physical CPUs are counted.
            verbose(bool): Controls whether to print the progress of each job
                into to the console.

        Returns:
            If return_descriptor is True, returns a tuple, where the first item
            is the derivative array and the second is the descriptor array.
            Otherwise only returns the derivatives array. The derivatives array
            is a either a 4D or 5D array, depending on whether you have
            provided a single or multiple systems. If the output shape for each
            system is the same, a single monolithic numpy array is returned.
            For variable sized output (e.g. differently sized systems,
            different number of centers or different number of included atoms),
            a regular python list is returned. The dimensions are:
            [(n_systems,) n_positions, n_atoms, 3, n_features]. The first
            dimension goes over the different systems in case multiple were
            given. The second dimension goes over the descriptor centers in
            the same order as they were given in the argument. The third
            dimension goes over the included atoms. The order is same as the
            order of atoms in the given system. The fourth dimension goes over
            the cartesian components, x, y and z. The fifth dimension goes over
            the features in the default order.
        """

        # Fix some parameters
        def derivatives_single_f(system, return_descriptor):
            return self.derivatives_single(
                system,
                return_descriptor,
                return_atomic_derivatives=return_atomic_derivatives,
                return_strain_derivatives=return_strain_derivatives,
            )

        # If single system given, skip the parallelization
        if isinstance(system, (Atoms, System)):
            return derivatives_single_f(
                system,
                return_descriptor=return_descriptor,
            )

        n_samples = len(system)

        # Combine input arguments
        inp = list(
            zip(
                system,
                [return_descriptor] * n_samples,
            )
        )

        n_features = self.get_number_of_features()

        if return_atomic_derivatives and return_strain_derivatives:
            derivatives_shape = (self.atoms_per_system + 2, 3, n_features)
        elif return_atomic_derivatives:
            derivatives_shape = (self.atoms_per_system, 3, n_features)
        elif return_strain_derivatives:
            derivatives_shape = (2, 3, n_features)
        else:
            raise ValueError(
                "Either return_atomic_derivatives or return_strain_derivatives has to be True"
            )

        descriptor_shape = (n_features,)

        # Create in parallel
        output = self.derivatives_parallel(
            inp,
            derivatives_single_f,
            n_jobs,
            derivatives_shape,
            descriptor_shape,
            return_descriptor,
            only_physical_cores,
            verbose=verbose,
        )

        return output

    def derivatives_single(
        self,
        system,
        return_descriptor=True,
        return_atomic_derivatives=True,
        return_strain_derivatives=False,
    ):
        """Calculate the MBTR derivatives for the given system.

        Args:
            system (:class:`ase.Atoms`): Atomic structure.
            return_descriptor (bool): Whether to also calculate the descriptor
                in the same function call. This is true by default as it
                typically is faster to calculate both in one go.

        Returns:
            If return_descriptor is True, returns a tuple, where the first item
            is the derivative array and the second is the descriptor array.
            Otherwise only returns the derivatives array. The derivatives array
            is a 3D numpy array. The dimensions are: [n_atoms, 3, n_features].
            The first dimension goes over all the atoms in the system. The
            second dimension goes over the cartesian components, x, y and z.
            The last dimension goes over the features in the default order.
        """

        # Ensuring variables are re-initialized when a new system is introduced
        self.system = system
        self._interaction_limit = len(system)

        # Check that the system does not have elements that are not in the list
        # of atomic numbers
        self.check_atomic_numbers(system.get_atomic_numbers())

        weighting = self.k2["weighting"]
        weight_func = weighting["function"]
        if weight_func in ["exponential", "exp"]:
            cutoff = weighting["threshold"]
            if "r_cut" in weighting:
                radial_cutoff = weighting["r_cut"]
                if radial_cutoff != 0:
                    scale = -np.log(cutoff) / radial_cutoff
            else:
                scale = weighting["scale"]
                if scale != 0:
                    radial_cutoff = -np.log(cutoff) / scale
        else:
            scale = None
            cutoff = None
            radial_cutoff = None

        # If needed, create the extended system
        if self.periodic:
            centers = system.get_positions()
            ext_system, cell_indices = dscribe.utils.geometry.get_extended_system(
                system, radial_cutoff, centers, return_cell_indices=True
            )
            ext_system = System.from_atoms(ext_system)
        else:
            ext_system = system
            cell_indices = np.zeros((len(system), 3), dtype=int)

        n_atoms = len(ext_system)
        if radial_cutoff is not None:
            dmat = ext_system.get_distance_matrix_within_radius(radial_cutoff)
            adj_list = dscribe.utils.geometry.get_adjacency_list(dmat)

            # Padding the nested list into 2d ndarray for Numba
            max_len = max(len(l) for l in adj_list)
            adj_mat = np.full((len(adj_list), max_len), -1, dtype=int)
            for i, l in enumerate(adj_list):
                adj_mat[i, : len(l)] = l
        else:
            adj_mat = np.tile(np.arange(n_atoms), (n_atoms, 1))

        grid = self.k2["grid"]
        n = grid["n"]

        positions = ext_system.get_positions()
        Z = ext_system.get_atomic_numbers()
        element_index = np.array(
            [self.atomic_number_to_index[Z[i]] for i in range(len(Z))]
        )

        # Generate MBTR_k2^{Z_1,Z_2} map and its derivatives
        k2_map, k2_atomic_derivative_map, k2_strain_derivative_map = get_k2_maps(
            len(system),
            adj_mat,
            positions,
            self.k2["geometry"]["function"],
            weight_func,
            scale,
            cutoff,
            grid["n"],
            grid["min"],
            grid["max"],
            grid["sigma"],
            cell_indices,
            Z,
            element_index,
            return_atomic_derivatives,
            return_strain_derivatives,
        )

        n_elem = self.n_elements
        n_features = self.get_number_of_features()
        n_atoms = self.atoms_per_system
        pairs = int(n_elem * (n_elem + 1) / 2)

        k2 = np.zeros((n_features), dtype=np.float32)
        if return_atomic_derivatives and return_strain_derivatives:
            k2_d = np.zeros((n_atoms + 2, 3, n_features), dtype=np.float32)
        elif return_atomic_derivatives:
            k2_d = np.zeros((n_atoms, 3, n_features), dtype=np.float32)
        elif return_strain_derivatives:
            k2_d = np.zeros((2, 3, n_features), dtype=np.float32)

        for key, gaussian_sum in sorted(k2_map.items()):
            i = key[0]
            j = key[1]

            # This is the index of the spectrum. It is given by enumerating the
            # elements of an upper triangular matrix from left to right and top
            # to bottom.
            m = int(j + i * n_elem - i * (i + 1) / 2)

            # Denormalize if requested
            if not self.normalize_gaussians:
                max_val = 1 / (sigma * math.sqrt(2 * math.pi))
                gaussian_sum /= max_val
                k2_atomic_derivative_map[key] /= max_val
                k2_strain_derivative_map[key] /= max_val

            # if self.flatten:
            start = m * n
            end = (m + 1) * n
            k2[start:end] = gaussian_sum

            if return_atomic_derivatives:
                k2_d[:n_atoms, :, start:end] = k2_atomic_derivative_map[key]
            if return_strain_derivatives:
                k2_d[-2:, :, start:end] = k2_strain_derivative_map[key].reshape(
                    (2, 3, -1)
                )

        # Handle normalization
        if self.normalization == "l2_each":
            # This is incorrect. norm is not constant
            # norm = np.linalg.norm(k2)
            # k2 /= norm
            # k2_d /= norm
            pass

        elif self.normalization == "n_atoms":
            k2 /= n_atoms
            k2_d /= n_atoms

        # Convert to sparse here. Currently everything up to this point is
        # calculated with dense matrices. This imposes some memory limitations.
        if self.sparse:
            k2 = sparse.COO.from_numpy(k2)
            k2_d = sparse.COO.from_numpy(k2_d)

        if return_descriptor:
            return (k2_d, k2)
        return k2_d
