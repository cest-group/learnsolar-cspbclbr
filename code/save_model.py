import pickle

from model import *


def save_model_uniform_data():
    """Fits the ML model with optimal hyperparameters and saves it."""
    model = optimized_uniform_data_model(fit=True)
    model_file = "../models/uniform_data.bin"
    print("Dumping the model to ", model_file)
    pickle.dump(model, open(model_file, "wb"))


if __name__ == "__main__":
    save_model_uniform_data()
