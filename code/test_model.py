import matplotlib.pyplot as plt

from load_data import *
from init_mbtr import *
from load_model import *
from energy_transformation import *
from merge_data import *


def test_energy_prediction(model):
    """
    Tests the energy prediction accuracy of the given ML model on the test set.
    Args:
        model (Model object): ML model
    """

    print("Testing ML model energy prediction:")

    s, l = merged_uniform_data()
    et = l["total_energy"].values

    aes = []

    for phase in ["i4mcm", "p4mbm", "pm3m", "pnma"]:
        te_mask = (l["lattice_type"] == phase) & (l["train"] == False)
        s_te = [s[i] for i in range(len(s)) if te_mask[i]]
        e_te = et[te_mask]

        e_te = np.array(e_te)
        e_pr = model.predict_from_structure(s_te)

        e_te = 1000 / 8 * EnthalpyOfMixingTransformation().transform(e_te, s_te)
        e_pr = 1000 / 8 * EnthalpyOfMixingTransformation().transform(e_pr, s_te)

        mae = np.mean(np.abs(e_te - e_pr))
        print("  {}, MAE_E = {:.2f} meV/f.u.".format(phase, mae))

        aes += list(np.abs(e_te - e_pr))

        mi = min(np.min(e_te), np.min(e_pr))
        ma = max(np.max(e_te), np.max(e_pr))
        r = ma - mi
        mi -= 0.1 * r
        ma += 0.1 * r

        fig, ax = plt.subplots()
        fig.set_figheight(4)
        fig.set_figwidth(4)

        plt.title("{}, MAE = {:.2f} meV/f.u.".format(phase, mae))
        plt.plot(e_te, e_pr, "o")
        plt.plot([mi, ma], [mi, ma], "-k")
        plt.xlabel("$\Delta H_{mix}^{DFT}$ (meV/f.u.)")
        plt.ylabel("$\Delta H_{mix}^{ML}$ (meV/f.u.)")

        ax.set_aspect("equal")
        plt.tight_layout()

    print("  All, MAE_E = {:.2f} meV/f.u.".format(np.mean(aes)))

    plt.show()


def test_initial_force_prediction(model):
    """
    Tests the force prediction accuracy of the given ML model on the initial structures in the test set.
    Args:
        model (Model object): ML model
    """

    print("Testing ML model force prediction on initial structures:")

    s, l = merged_uniform_data()

    maes = []
    for phase in ["i4mcm", "p4mbm", "pm3m", "pnma"]:
        te_mask = (
            (l["lattice_type"] == phase)
            & (l["train"] == False)
            & (l["relaxation_iteration"] == 0)
        )
        s_te = [s[i] for i in range(len(s)) if te_mask[i]]

        f_te = np.array([st.get_forces() for st in s_te])
        f_pr = -model.predict_from_structure(s_te, ["atomic_derivatives"])

        mae = np.mean(np.abs(f_te.flatten() - f_pr.flatten())) * 1000
        maes.append(mae)
        print("  {}, MAE_F = {:.2f} meV/Å".format(phase, mae))

        mi = min(np.min(f_te), np.min(f_pr))
        ma = max(np.max(f_te), np.max(f_pr))
        r = ma - mi
        mi -= 0.1 * r
        ma += 0.1 * r

        fig, ax = plt.subplots()
        fig.set_figheight(4)
        fig.set_figwidth(4)

        plt.plot(f_te.flatten(), f_pr.flatten(), "o")
        plt.plot([mi, ma], [mi, ma], "-k")

        plt.title("{}, MAE = {:.2f} meV/Å".format(phase, mae))
        plt.xlabel("$F_i^{DFT}$ (meV/Å)")
        plt.ylabel("$F_i^{ML}$ (meV/Å)")

        ax.set_aspect("equal")
        plt.tight_layout()

    print("  All, MAE_F = {:.2f} meV/Å".format(np.mean(maes)))
    plt.show()


if __name__ == "__main__":
    model = load_model_uniform_data()

    test_energy_prediction(model)
    test_initial_force_prediction(model)
