from load_data import *


def merged_uniform_single_point_data(data_path="../data/", shuffle=True):
    """
    Merges the single point data sets. Drops out some structures randomly so
    that the resulting data set is uniform with regard to the Cl concentration.
    Args:
        data_path (string): relative path to the data directory
        shuffle (boolean): if 'True', shuffles the resulting data set
    Returns:
        list: list of ASE Atoms objects
        Pandas DataFrame: data frame containing the properties
    """

    pick_rng = np.random.RandomState(0)
    shuffle_rng = np.random.RandomState(1)

    s_binomial, l_binomial = load_cspbclbr_single_point(data_path)
    s_tails, l_tails = load_cspbclbr_single_point_tails(data_path)

    s_all = s_binomial + s_tails
    l_all = pd.concat([l_binomial, l_tails], ignore_index=True)

    n_cl = np.array([np.sum(st.get_atomic_numbers() == 17) for st in s_all])
    i_all = np.arange(len(s_all))

    s_merge = {}
    l_merge = {}
    i_merge = []
    for phase in ["i4mcm", "p4mbm", "pm3m", "pnma"]:
        i_merge_phase = []
        for N_cl in range(25):
            mask = np.logical_and(l_all["lattice_type"] == phase, n_cl == N_cl)
            random_selection = pick_rng.choice(np.sum(mask), 100, replace=False)
            indices = i_all[mask][random_selection]
            i_merge_phase += list(indices)

        if shuffle:
            shuffle_rng.shuffle(i_merge_phase)
        i_merge += i_merge_phase

    s_merge = [s_all[i] for i in i_merge]
    l_merge = l_all.take(i_merge)

    l_merge.reset_index(drop=True, inplace=True)

    return s_merge, l_merge


def merged_uniform_data(data_path="../data/"):
    """
    Costructs a data set that has single point structures and relaxations.
    Splits the set into training and test sets and saves the information in the
    returned data frame with key 'train' (True or False). Similarly, splits
    the data into 5 parts in preparation for cross validation, and saves the
    splits with key 'cv_split' (0, 1, 2, 3, or 4).
    Args:
        data_path (string): relative path to the data directory
    Returns:
        list: list of ASE Atoms objects
        Pandas DataFrame: data frame containing the properties
    """

    s_s, l_s = merged_uniform_single_point_data(data_path)
    s_r, l_r = load_cspbclbr_relax_uniform(data_path)

    s = s_s + s_r
    l = pd.concat([l_s, l_r], ignore_index=True)

    l["train"] = len(s_s) * [True] + list(l_r["relaxation_number"] % 2 == 1)
    l["cv_split"] = [i % 5 for i in range(len(s_s))] + list(
        ((l_r["relaxation_number"] - 1) // 2) % 5
    )
    l.loc[l["train"] == False, "cv_split"] = -1

    return s, l
