import numpy as np
import pandas as pd
import itertools
import ase.io


def load_cspbclbr_single_point(data_path="../data/"):
    """
    Loads a data set of CsPb(Cl/Br)3 atomic structures and their properties:
        "total_energy": DFT total energy
        "lattice_type": lattice type of the structure (pm3m, p4mbm, i4mcm, or pnma)
    Args:
        data_path (string): relative path to the data folder
    Returns:
        list: list of ASE Atoms objects
        Pandas DataFrame: data frame containing the properties
    """
    dir_path = data_path + "/cspbclbr_single_point/"

    phases = ["i4mcm", "p4mbm", "pm3m", "pnma"]

    structures = []
    properties = {}
    properties["total_energy"] = []
    properties["lattice_type"] = []

    for phase in phases:
        s = ase.io.read("{0}/{1}/structures.xyz".format(dir_path, phase), index=":")
        structures += s
        properties["total_energy"] += list(
            np.loadtxt("{0}/{1}/etot.dat".format(dir_path, phase))
        )
        properties["lattice_type"] += len(s) * [phase]

    return structures, pd.DataFrame(data=properties)


def load_cspbclbr_single_point_tails(data_path="../data/"):
    """
    Loads a data set of CsPb(Cl/Br)3 atomic structures and their properties:
        "total_energy": DFT total energy
        "lattice_type": lattice type of the structure (pm3m, p4mbm, i4mcm, or pnma)
    Args:
        data_path (string): relative path to the data folder
    Returns:
        list: list of ASE Atoms objects
        Pandas DataFrame: data frame containing the properties
    """
    dir_path = data_path + "/cspbclbr_single_point_tails/"

    phases = ["i4mcm", "p4mbm", "pm3m", "pnma"]

    structures = []
    properties = {}
    properties["total_energy"] = []
    properties["lattice_type"] = []

    for phase in phases:
        s = ase.io.read("{0}/{1}/structures.xyz".format(dir_path, phase), index=":")
        structures += s
        properties["total_energy"] += list(
            np.loadtxt("{0}/{1}/etot.dat".format(dir_path, phase))
        )
        properties["lattice_type"] += len(s) * [phase]

    return structures, pd.DataFrame(data=properties)


def load_cspbclbr_relax_uniform(data_path="../data/"):
    """
    Loads a data set of CsPb(Cl/Br)3 atomic structures and their properties:
        "relaxation_number": number of the DFT relaxation matching the file name in the data set
        "relaxation_iteration": iteration number within the DFT relaxation
        "relaxed": tag indicating whether a structure is fully relaxed (True) or not (False)
        "total_energy": DFT total energy
        "lattice_type": lattice type of the structure (pm3m, p4mbm, i4mcm, or pnma)
    Args:
        data_path (string): relative path to the data folder
    Returns:
        list: list of ASE Atoms objects
        Pandas DataFrame: data frame containing the properties
    """
    dir_path = data_path + "/cspbclbr_relax_uniform/"

    structures = []
    properties = {}
    properties["relaxation_number"] = []
    properties["relaxation_iteration"] = []
    properties["relaxed"] = []
    properties["total_energy"] = []
    properties["lattice_type"] = []

    for phase in ["i4mcm", "p4mbm", "pm3m", "pnma"]:
        for i in range(3001, 3051):
            path = "{}/{}/{}0{}".format(dir_path, phase, phase, i)
            s = ase.io.read(path + ".xyz", index=":")
            structures += s
            properties["relaxation_number"] += len(s) * [i]
            properties["relaxation_iteration"] += list(range(len(s)))
            properties["total_energy"] += list(
                np.loadtxt(path + "_etot.dat".format(dir_path, phase))
            )
            properties["lattice_type"] += len(s) * [phase]
            properties["relaxed"] += (len(s) - 1) * [False] + [True]

    return structures, pd.DataFrame(data=properties)
