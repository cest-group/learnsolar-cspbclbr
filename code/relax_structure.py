import copy

from ase.optimize import BFGS
from ase.constraints import UnitCellFilter, ExpCellFilter

from ml_calculator import *


def relax_structure(
    structure,
    model,
    force_limit=5e-3,
    bfgs_maxstep=0.1,
    relax_cell=False,
    steps=1000000,
    trajectory_file=None,
):
    """
    Relaxes the atomic positions and optionally the lattice parameters of a
    structure using BFGS with the ML model.
    Args:
        structure (ASE Atoms object): Atomic structure to relax
        model (Model object): ML model
        force_limit (float): converge limit for maximum force component
        bfgs_maxstep (float): maximum amount that the atoms can move on one step in angstroms
        relax_cell (boolean): If 'True', relax the lattice parameters.
        steps (int): maximum number of iterations
        trajectory_file (string): path for the trajectory file
    Returns:
        ASE Atoms object: relaxed geometry
        MLCalculator object: ASE calculator that can be used to access e.g. the predicted energies along the relaxation trajectry.
    """
    s = copy.deepcopy(structure)
    calc = ML_Calculator(model)
    s.set_calculator(calc)

    if not relax_cell:
        if trajectory_file:
            dyn = BFGS(
                s, logfile=None, maxstep=bfgs_maxstep, trajectory=trajectory_file
            )
        else:
            dyn = BFGS(s, logfile=None, maxstep=bfgs_maxstep)
    else:
        filt = UnitCellFilter(s)
        if trajectory_file:
            dyn = BFGS(
                filt, logfile=None, maxstep=bfgs_maxstep, trajectory=trajectory_file
            )
        else:
            dyn = BFGS(filt, logfile=None, maxstep=bfgs_maxstep)
    dyn.run(fmax=force_limit, steps=steps)

    return s, calc
